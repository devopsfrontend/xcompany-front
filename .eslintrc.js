module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/recommended',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': 0,
    'no-debugger': 0,
    'no-plusplus': 0,
    'import/extensions': ['error', {
      'js': 'never',
      'vue': 'always',
      'json': 'always',
    }],
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: ['state'],
    }],
    'vue/html-closing-bracket-newline': ['error', {
      'singleline': 'never',
      'multiline': 'never',
    }],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};
