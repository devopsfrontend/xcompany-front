/* eslint-disable no-else-return */
import store from '@/store';

export default {
  isUserAuthenticated(to, from, next) {
    const { isAuthenticated } = store.getters;
    if (isAuthenticated) {
      next();
      return;
    }

    // User not authenticated, check for protected routes
    if (to.matched.some((route) => route.meta.requiresAuth)) {
      next('/home');
    } else {
      next();
    }
  },
};
