import Vue from 'vue';
import VueRouter from 'vue-router';

import navigationGuards from './navigationGuards';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      redirect: {
        name: 'home',
      },
    },
    {
      path: '/home',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ '@/pages/Home.vue'),
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ '../pages/About.vue'),
    },
    {
      path: '/services',
      name: 'services',
      component: () => import(/* webpackChunkName: "services" */ '../pages/Services.vue'),
    },
    {
      path: '/my-account',
      name: 'myAccount',
      component: () => import(/* webpackChunkName: "my-account" */ '../pages/MyAccount.vue'),
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach(navigationGuards.isUserAuthenticated);

export default router;
