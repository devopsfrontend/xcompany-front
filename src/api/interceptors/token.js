export default {
  request: (request) => {
    const user = JSON.parse(localStorage.getItem('xcompany-user'));
    if (user?.token) {
      // eslint-disable-next-line no-param-reassign
      request.headers.Authorization = `Bearer ${user.token}`;
    }

    return request;
  },
  requestError: (err) => Promise.reject(err),
  response: (response) => response,
  responseError: (err) => {
    const { response } = err;

    // Need unique data.message to check "Expired token"
    if (response && response.status === 401) {
      localStorage.removeItem('xcompany-user');
    }

    return Promise.reject(err);
  },
};
