import axios from 'axios';
import interceptors from './interceptors';

const http = axios.create({
  baseURL: process.env.VUE_APP_API_BASEURL,
  timeout: 5000,
});

http.interceptors.request.use(
  interceptors.token.request,
  interceptors.token.requestError,
);

http.interceptors.response.use(
  interceptors.token.response,
  interceptors.token.responseError,
);

export default http;
