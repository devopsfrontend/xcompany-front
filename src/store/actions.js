import api from '@/api/http';
import homeData from './data/home.json';
import aboutData from './data/about.json';
import servicesData from './data/services.json';

export default {
  loginCheck({ dispatch }) {
    api.get('/api/users/loginCheck').catch(() => {
      dispatch('logout');
    });
  },
  register({ commit }, userData) {
    return new Promise((resolve, reject) => {
      api.post('/api/users/signup', userData).then((response) => {
        commit('login', response.data);
        resolve();
      }).catch((err) => {
        reject(err);
      });
    });
  },
  login({ commit }, loginData) {
    return new Promise((resolve, reject) => {
      api.post('/api/users/authenticate', loginData).then((response) => {
        commit('login', response.data);
        resolve();
      }).catch((err) => {
        reject(err);
      });
    });
  },
  logout({ commit }) {
    return new Promise((resolve) => {
      commit('logout');
      resolve();
    });
  },
  getHomeData({ commit }) {
    commit('setHomeData', homeData);
  },
  getAboutData({ commit }) {
    commit('setAboutData', aboutData);
  },
  getServicesData({ commit }) {
    commit('setServicesData', servicesData);
  },
  sendContactForm(state, formData) {
    return api.post('/api/ContactLogs', formData);
  },
  getProducts({ commit }) {
    return api.get('/api/products').then((response) => {
      commit('setProducts', response.data);
    });
  },
  getUserProducts({ commit }, userId) {
    return api.get(`api/orders/${userId}`).then((response) => {
      commit('setUserProducts', response.data);
    });
  },
  selectProduct({ commit }, product) {
    commit('selectProduct', product);
  },
  removeProduct({ commit }, product) {
    commit('removeProduct', product);
  },
  toggleShop({ commit }, isOpen) {
    commit('toggleShop', isOpen);
  },
  emptySelectedProducts({ commit }) {
    commit('emptySelectedProducts');
  },
  saveProducts(state, data) {
    return api.post('/api/orders/Buy', data);
  },
};
