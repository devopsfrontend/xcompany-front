export default {
  isAuthenticated: (state) => !!state.user.token,
  totalPrice: (state) => {
    let total = 0;

    state.shop.selectedProducts.forEach((product) => {
      total += (product.quantity * product.price);
    });

    return total;
  },
};
