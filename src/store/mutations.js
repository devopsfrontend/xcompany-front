export default {
  login(state, user) {
    state.user = user;
    localStorage.setItem('xcompany-user', JSON.stringify(state.user));
  },
  logout(state) {
    state.user = {};
    localStorage.removeItem('xcompany-user');
  },
  setHomeData(state, homeData) {
    state.home = homeData;
  },
  setAboutData(state, aboutData) {
    state.about = aboutData;
  },
  setServicesData(state, servicesData) {
    state.services = servicesData;
  },
  setProducts(state, products) {
    state.shop.products = products || [];
  },
  setUserProducts(state, userProducts) {
    state.shop.userProducts = [];
    userProducts.forEach((userProduct) => {
      userProduct.products.forEach((product) => {
        state.shop.userProducts.push(product);
      });
    });
  },
  selectProduct(state, product) {
    const found = state.shop.selectedProducts
      .find((selected) => selected.productId === product.productId);
    if (!found) {
      // eslint-disable-next-line no-param-reassign
      product.quantity = 1;
      state.shop.selectedProducts.push(product);
    }
  },
  removeProduct(state, product) {
    const selected = state.shop.selectedProducts;
    for (let i = 0; i < selected.length; i++) {
      if (selected[i].productId === product.productId) {
        selected.splice(i, 1);
        break;
      }
    }
  },
  toggleShop(state, isOpen) {
    state.shop.isOpen = isOpen;
  },
  emptySelectedProducts(state) {
    state.shop.selectedProducts = [];
  },
};
