export default {
  user: JSON.parse(localStorage.getItem('xcompany-user')) || {},
  config: {
    baseURL: process.env.VUE_APP_API_BASEURL,
  },
  home: {},
  about: {},
  services: {},
  shop: {
    isOpen: false,
    products: [],
    selectedProducts: [],
    userProducts: [], // Bought products
  },
};
