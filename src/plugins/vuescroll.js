import Vue from 'vue';
import vuescroll from 'vuescroll';

Vue.use(vuescroll, {
  ops: {
    vuescroll: {},
    scrollPanel: {},
    rail: {},
    bar: {
      background: '#009b9d', // Can be overwritten as scss variable
      onlyShowBarOnScroll: false,
    },
  },
});
