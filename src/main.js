import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

// Import custom globally plugins and directives
import 'bootstrap';
import './plugins';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  beforeCreate() {
    this.$store.dispatch('loginCheck');
  },
  render: (h) => h(App),
}).$mount('#app');
